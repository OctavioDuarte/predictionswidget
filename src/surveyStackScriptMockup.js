const fs = require('fs');
const fetch = require('node-fetch');

let survey = JSON.parse( fs.readFileSync( "../exampleSurvey/exampleEntriesFromSurveyS.json", "utf8" ) );
let submission = survey[0];

let context = {};

function dig (object, path, defaultValue = null) {
    const chunks = path.split('.');
    try {
        const value = chunks.reduce((current, chunk) => current[chunk], object);
        if (value !== undefined) {
            return value;
        } else {
            return defaultValue;
        }
    } catch (e) {
        return defaultValue;
    }
};

const wavelengths = [ // wavelength order for calibration measurement
  850,
  880,
  365,
  385,
  450,
  530,
  587,
  632,
  500,
  940,
]

const cropColors = [
  "beige",
  "black",
  "blue",
  "purple",
  "brown",
  "green",
  "orange",
  "red",
  "white",
  "yellow"
]
;

/*
* Generates a name that is used to get information from the survey object.
* output looks like data.scans.scan_[iteration].value.median_[wavelength]
* iteration: what iteration are we interested in. Currently, 1 to 3.
* wavelength: wavelength we are searching for. These are registered in the wavelengths object.
*/
function scanNameByIterationAndWavelength(iteration, wavelength) {
  return `data.scans.scan_${iteration}.value.median_${wavelength}`;
};

/*
* Generates the name that the model is expecting, based on the wavelength.
* The information sent should be the average of three previous scans.
* wavelength: the wavelength that is being sent.
*/
function scanNameRequiredByTheModel(wavelength) {
  return `surface.scan_${wavelength}`;
};

/*
* Retrieves what's needed from the context object and consolidates the input this particular model expects.
*/
function modelInputCNoVisWho( context ) {
  let predictionVariablesObject = {
  "surface.scan_365": context["surface.scan_365"],
  "surface.scan_385": context["surface.scan_385"],
  "surface.scan_450": context["surface.scan_450"],
  "surface.scan_500": context["surface.scan_500"],
  "surface.scan_530": context["surface.scan_530"],
  "surface.scan_587": context["surface.scan_587"],
  "surface.scan_632": context["surface.scan_632"],
  "surface.scan_850": context["surface.scan_850"],
  "surface.scan_880": context["surface.scan_880"],
  "surface.scan_940": context["surface.scan_940"],
  "dataset": "c-no-vis-who",
  "Type":context["Type"]
  };
  cropColors.forEach( color => {
    //predictionVariablesObject[`color.${color}`] = context.cropColor[color];    
    predictionVariablesObject[`color.${color}`] = false;
    // TESTING, change!!
    predictionVariablesObject[`color.orange`] = true;  
  } );
  return predictionVariablesObject;
};

/*
* Gets all the scan iterations for each wavelengths, averages and feeds the results with adequate varnames to the context.
*/
function feedMediansToContext(contextObj, surveyObj, wavelengthsList = wavelengths, iterations = [1, 2, 3]) {
  wavelengthsList.forEach( wave => {
    let rawScanVarnames = iterations.map(iter => scanNameByIterationAndWavelength(iter, wave));
      let rawScans = rawScanVarnames.map(varname => parseFloat( dig( surveyObj, varname ) ) );
      let average = rawScans.reduce( (a,b) => a + b ) / rawScans.length;
      let modelVarname = scanNameRequiredByTheModel( wave );
    contextObj[ modelVarname ] = average;
  });
};

function feedColorsToContext( surveyObj, contextObj, colorsList = cropColors ) {
    let crop = surveyObj.data.metadata.crop_type.value[0];
    let colorsPath = `data.metadata.${crop}_color.value`;
    let sampleColorsArray = dig( surveyObj, colorsPath );
  colorsList.forEach( color => {
    if ( sampleColorsArray.includes( color ) ) {
      contextObj[ `color.${color}` ] = true;
    } else {
        contextObj[ `color.${color}` ] = false;
    }
        ;
  } );
};


feedMediansToContext( context, submission,  );
feedColorsToContext( submission, context );
context["Type"] = submission.data.metadata.crop_type.value[0];

// /**
//  * Process
//  * @param {props} props
//  * @param {submission} props.submission
//  * @param {state} state
//  */
// export async function process(props, state) {
//   const { submission, control, params } = props;
//   const { value, context } = state;

//   feedMediansToContext( context, survey );
//   feedColorsToContext( submission, context )  
//   context["Type"] = submission.data.crop_type;
//   // do stuff
//   // ...
//   return {
//     context,
//     value: { ...context },
//     status: {
//       type: statusTypes.SUCCESS,
//       message: `script successfully executed at 2021-05-03T19:33:45.913Z`,
//     },
//   }
// }


let dataExCarrot = {
  //"juice.scan_365":1513.78,
  //"juice.scan_385":1300.78,
  //"juice.scan_450":1040.21,
  //"juice.scan_500":969.981,
  //"juice.scan_530":1290.46,
  //"juice.scan_587":3691.75,
  //"juice.scan_632":3962.02,
  //"juice.scan_850":4708.08,
  //"juice.scan_880":4126.99,
  //"juice.scan_940":6044.45,
  "surface.scan_365": 402.8375,
  "surface.scan_385": 464.36,
  "surface.scan_450": 555.803,
  "surface.scan_500": 549.2385,
  "surface.scan_530": 608.0045,
  "surface.scan_587": 1051.84,
  "surface.scan_632": 1161.87,
  "surface.scan_850": 1771.395,
  "surface.scan_880": 1332.65,
  "surface.scan_940": 2262.38,
  "Type": "carrot",
  "color.orange": true,
  "color.red": false,
  "color.yellow": false,
  "color.brown": false,
  "color.purple": false,
  "color.green": false,
  "color.blue": false,
  "color.white": false,
  "color.beige": false,
  "dataset": "c-no-vis-who"
};

const url = "https://predict.surveystack.io/qrf_numbers";

const modelData = modelInputCNoVisWho( context );

/*
* sends the information to the server, gets the intervals and plots them
* @predictorVariablesObject is a JSON object with all the information needed by the model.
* @individualWidth is the width of one subplot (each interval). Typically the full width of the container div.
* @individualHeight is the height of the intervals. The 
* @ containerID is the CSS id of the div where the plot should be added.
* @ widgetID is the ID that the main div of the visualization will have
*/
const request = new fetch.Request(url, {
method: 'POST',
body: JSON.stringify(modelData),
headers: new fetch.Headers({
    'Content-Type': 'application/json',
    "Access-Control-Request-Method": "POST"
})
});

fetch(request)
.then(res => res.json())
.then(rawResponse => {
    console.log(rawResponse);
    //let parsedResponse = rawResponse.map( function(prediction) {
    //  Object.keys( prediction ).forEach( key => prediction[key] = prediction[key][0] )
    //} )
    // drawPredictionWidget(rawResponse, individualWidth, individualHeight, containerID, widgetID);
})
.catch(err => {
    console.log("Error:", err);
})
;
