let exampleResponse = {
    prediction:0.11,
    min:0.070,
    max:0.26,
    variable: "Polyphenols",
    confidence: 80
}
;

let exampleResponse2 = {
    prediction:0.6,
    min:0.3,
    max:0.8,
    variable: "Proteins",
    confidence: 80
}
;

let exampleResponse3 = {
    prediction:0.3,
    min:0.2,
    max:0.51,
    variable: "Antioxidants",
    confidence: 80
}
;

const colorScale = d3.scaleSequential(d3.interpolateViridis)
      .domain([0,1])
;


const variableDescriptions = {
    BQI: "This is a quality index which ponders mineral and nutritional content on a given species. It includes Antioxidants, Porlpyphenols, Brix (except on grain), Protein (just on grain) and nutritionally relevant and scarce enough minerals (Mg, S, K, Ca, Fe, Zn)",
    Antioxidants: "Antioxidants are considered relevant in a healthy diet.",
    Polyphenols: "Polyphenols are important indicators of produce quality and health.",
    Proteins: "Proteins are one of the main nutrients we get from grain. They are not just an important part of a healthy diet, but also the defining factor in the quality of obtained bread."
}

/*
 * @data is the response from our predction API, it contains max and min to define the interval, prediction for te central value and the predicted var name indicated as `variable`
 */
function drawPredictionRange( data, width, height, containerID ) {

    const colorScale = d3.scaleSequential(d3.interpolateViridis)
          .domain([0,1])
    ;

    let div = d3.select( `#${containerID}` )
        .append("div")
        .style("width", width)
        .style( "color", "black" )
        .style( "border-radius", "5px" )
        .style("box-shadow","1px 1px 1px 0.5px rgba(0,0,0,0.1)")
        .style("margin","3px")
    ;

    let svg = div.append("svg")
        .attr( "height", height )
        .attr( "width", width )
    ;

    // big display
    svg.append( "circle" )
        .attr("cx", `${width/8}px` )
        .attr("cy",`${height/2}px`)
        .attr("r",`${height/3}px`)
        .attr("fill", colorScale( 1 - 0.6 * data.prediction ) )
    ;
    svg.append("text")
        .attr("x", `${width/8}px` )
        .attr("y",`${height/1.9}px`)
        .style("text-anchor", "middle")
        .style("dominant-baseline", "middle")
        .style( "font-size", `${ height * 0.4 }px` )
        .style( "font-family", `sans-serif` )
        .style("fill", "white")
        .text( Math.round( data.prediction * 100 ) )
    ;

    //variable tag
    let varTag = svg.append( "text" )
    // .attr("x", `${100/8 + 12.5}%` )
        .attr("x", `${ 0.23 * width}px` )
        .attr("y",`${ 0.35 * height}px`)
        .text( data.variable )
        .style( "font-size", `${ height * 0.15 }px` )
        .style( "font-family", `sans-serif` )
        .style("cursor","help")
    ;

    // guide bar
    // let bar = svg.append( "rect" )
    //     .attr( "x", `${0.23 * width }px` )
    //     .attr( "y", `${ 0.45 * height }px` )
    //     .attr( "height", `${0.1*height}px` )
    //     .attr( "width", `${0.75 * width}px` )
    //     .style( "fill", "black" )
    // ;

    let barHeight = 0.1 * height;
    let barWidth = 0.5 * height;
    let barY = 0.45 * height;
    let barX = 0.23 * width;


    const xScale = d3.scaleLinear()
          .domain( [0,1] )
          .range( [ 0.23 * width, 0.23 * width + 0.73 * width ] )
    ;

    const gradientSteps = 100;

    let gradientG = svg.append("g");

    for ( let i = 0; i <= gradientSteps; i++ ) {
        let alpha = 1;
        let width = `${ 1.1 * ( xScale(2 * 1/gradientSteps) - xScale(1 * 1/gradientSteps) ) }px`;
        let xCoord = i * 1/gradientSteps;

        if ( xCoord < data.min ) {
            alpha = 0.3;
            width = `${ xScale(2 * 1/gradientSteps) - xScale(1 * 1/gradientSteps) }px`;
        } else if (xCoord > data.max) {
            alpha = 0.3;
            width = `${ xScale(2 * 1/gradientSteps) - xScale(1 * 1/gradientSteps) }px`;
        }

        gradientG.append( "rect" )
            .attr( "x", xScale( xCoord ) )
            .attr( "y", `${50 - 5}%` )
            .attr( "height", `${ barHeight }px` )
            .attr( "width", width )
            .style("fill", colorScale( 1 - i * (0.6/gradientSteps) ) )
            .style("opacity", alpha )
        ;
    }

    let diamondY = barY + 0.5 * barHeight;
    let diamondX = xScale( data.prediction );

    // interval extremes
    svg.append( "circle" )
        .attr( "cx", xScale( data.min ) )
        .attr("cy", diamondY)
        .attr( "r", barHeight * 0.9 )
        .attr( "fill", colorScale( 1 - 0.6 * data.min ) )
        .append("title")
        .text( "Minimum interval value." )
    ;

    svg.append( "circle" )
        .attr( "cx", xScale( data.max ) )
        .attr("cy",diamondY)
        .attr( "r", barHeight * 0.9 )
        .attr( "fill", colorScale( 1 - 0.6 * data.max ) )
        .append("title")
        .text( "Maximum interval value." )
    ;



    // diamond
    svg.append( "rect" )
        .attr( "x", diamondX - barHeight * 2.5 / 2 )
        .attr( "y", diamondY - barHeight * 2.5 / 2 )
        .attr( "height", barHeight * 2.5 )
        .attr( "width", barHeight * 2.5 )
        .style( "stroke-width", "1px" )
        .style( "stroke", "white" )
        .style( "fill", colorScale( 1 - 0.6 * data.prediction ) )
        .style( "transform-origin", `${diamondX}px ${diamondY}px` )
        .style( "transform", `rotate(45deg)` )
        .append("title")
        .text( "Predicted value." )
    ;

    let helpText = div.append("div")
        .style("display","none")
        .style("padding","10px")
    ;
    helpText.append("p")
        .text( variableDescriptions[data.variable] )
        .style( "font", `italic ${ height * 0.2 }px sans-serif` )
    ;

    varTag.on( "mouseover", () => {
        helpText
            .transition()
            .duration(2000)
            .style("display","block");
    } );

    varTag.on( "mouseout", () => {
        helpText
            .transition()
            .duration(2000)
            .style("display","none");
    } );
};

function drawSynthesisDisplay( data, width, height, containerID ) {
    let synthesis = {};
    let theresBQI = !data.every( d => d.variable !== "BQI" );

    if ( theresBQI ) {
        synthesis.value = data.find( d => d.variable == "BQI" ).prediction;
        synthesis.label = "BQI";
    } else {
        synthesis.value = data.reduce( (a,b) => ( b.prediction / data.length + a ) , 0 );
        synthesis.label = "Average";
    }
    ;

    const colorScale = d3.scaleSequential(d3.interpolateViridis)
          .domain([0,1])
    ;

    let div = d3.select( `#${containerID}` )
        .append("div")
        .style("height", height)
        .style("width", width)
        .style( "color", "black" )
        .style( "border-radius", "5px" )
        .style("box-shadow","1px 1px 1px 0.5px rgba(0,0,0,0.1)")
        .style("margin","3px")
    ;

    let svg = div.append("svg")
        .attr( "height", height )
        .attr( "width", width )
    ;

    // big display
    svg.append( "circle" )
        .attr("cx", `${width/2}px` )
        .attr("cy",`${height/2}px`)
        .attr("r",`${height/3}px`)
        .attr("fill", colorScale( 1 - 0.6 * synthesis.value ) )
    ;
    svg.append("text")
        .attr("x", `${width/2}px` )
        .attr("y",`${height/1.9}px`)
        .style("text-anchor", "middle")
        .style("dominant-baseline", "middle")
        .style( "font-size", `${ height * 0.4 }px` )
        .style( "font-family", `sans-serif` )
        .style("fill", "white")
        .text( Math.round( synthesis.value * 100 ) )
    ;
    //variable tag
    svg.append( "text" )
        .attr("x", `${width/2}px` )
        .attr("y",`${height/8}px`)
        .text( synthesis.label )
        .style( "font-size", `${ height * 0.15 }px` )
        .style( "font-family", `sans-serif` )
        .style("text-anchor", "middle")
        // .style("dominant-baseline", "middle")
    ;
};

// drawPredictionRange( exampleResponse, 150, 38, "container" );
// drawPredictionRange( exampleResponse2, 300, 75, "container" );
// drawPredictionRange( exampleResponse3, 300, 75, "container" );

function drawPredictionWidget( apiResponse, individualWidth, individualHeight, containerID, widgetID ) {
    // Panel
    let widgetDiv = d3.select( `#${containerID}` )
        .append("div")
        .style("width",`${ individualWidth }px`)
        .attr("id", widgetID )
        // .style("border-radius","5px")
        .style("box-shadow","1px 1px 1px 0.5px rgba(0,0,0,0.1)")
        .style( "padding", "4px" )
    ;
    //Title
    d3.select( `#${widgetID}` )
        .append("text")
        .style("font-family","sans-serif")
        .style( "font-size", `${ individualWidth / 10 }px` )
        .text("Results")
    ;
    // Syhntesis
    drawSynthesisDisplay( apiResponse, individualWidth, 2 * individualHeight, widgetID );
    // Details
    apiResponse.forEach( predictionObj => {
            drawPredictionRange( predictionObj, individualWidth, individualHeight, widgetID );
    } );
    widgetDiv.append("div")
        .append("text")
        .text( `${apiResponse[0].confidence}% confidence that the real value lies in the range provided.` )
        .style( "font", `italic ${ individualWidth / 30 }px sans-serif` )
    ;
};


let datosCarrot = '{"surface.scan_365":402.8375,"surface.scan_385":464.36,"surface.scan_450":555.803,"surface.scan_500":549.2385,"surface.scan_530":608.0045,"surface.scan_587":1051.84,"surface.scan_632":1161.87,"surface.scan_850":1771.395,"surface.scan_880":1332.65,"surface.scan_940":2262.38,"Type":"carrot","color.orange":true,"color.red":false,"color.yellow":false,"color.brown":false,"color.purple":false,"color.green":false,"color.blue":false,"color.white":false,"color.beige":false,"dataset":"c-no-vis-who"}';

let datosPepper = '{"surface.scan_365":293.677,"surface.scan_385":295.4925,"surface.scan_450":615.409,"surface.scan_500":662.248,"surface.scan_530":727.868,"surface.scan_587":709.524,"surface.scan_632":731.8215,"surface.scan_850":1241.8605,"surface.scan_880":927.963,"surface.scan_940":1555.495,"Type":"peppers","color.orange":false,"color.red":false,"color.yellow":true,"color.brown":false,"color.purple":false,"color.green":false,"color.blue":false,"color.white":true,"color.beige":false,"dataset":"c-no-vis-who"}';


let exampleWheat = '{"color.orange":[null],"color.red":[null],"color.yellow":[null],"color.brown":[null],"color.purple":[null],"color.green":[null],"color.blue":[null],"color.white":[null],"color.beige":[null],"whole.scan_365":[2446.86],"whole.scan_385":[2276.21],"whole.scan_450":[2670.17],"whole.scan_500":[2505.985],"whole.scan_530":[2649.41],"whole.scan_587":[3147.4],"whole.scan_632":[3553.335],"whole.scan_850":[7045.85],"whole.scan_880":[6131.645],"whole.scan_940":[8776.95],"Type":["wheat"],"dataset":["c-no-vis-who"]}';

let exampleCarrot = '{"color.orange":[true],"color.red":[false],"color.yellow":[false],"color.brown":[false],"color.purple":[false],"color.green":[false],"color.blue":[false],"color.white":[false],"color.beige":[false],"surface.scan_365":[490.515],"surface.scan_385":[547.2715],"surface.scan_450":[689.5195],"surface.scan_500":[613.487],"surface.scan_530":[686.638],"surface.scan_587":[1183.095],"surface.scan_632":[1223.475],"surface.scan_850":[1765.09],"surface.scan_880":[1261.01],"surface.scan_940":[2413.39],"Type":["carrot"],"dataset":["c-no-vis-who"]}';

let exampleZucchini = '{"color.orange":[false],"color.red":[false],"color.yellow":[false],"color.brown":[false],"color.purple":[false],"color.green":[true],"color.blue":[false],"color.white":[true],"color.beige":[false],"surface.scan_365":[401.453],"surface.scan_385":[386.059],"surface.scan_450":[354.5095],"surface.scan_500":[629.287],"surface.scan_530":[624.4015],"surface.scan_587":[583.8115],"surface.scan_632":[543.0605],"surface.scan_850":[4413.165],"surface.scan_880":[3654.485],"surface.scan_940":[4022.405],"Type":["zucchini"],"dataset":["c-no-vis-who"]}';
 

// const url = "http://192.168.0.80:8080/qrf_numbers";
// const url = "http://192.168.0.80:8080/qrf_random";

const url = "https://predict.surveystack.io/qrf_numbers";

const request = new Request(url, {
    method: 'POST',
    body: exampleWheat,
    headers: new Headers({
        'Content-Type': 'application/json',
        "Access-Control-Request-Method":"POST"
    })
});

fetch(request)
         .then( res => res.json() )
    .then( rawResponse => {
        console.log(rawResponse);
        drawPredictionWidget( rawResponse, 300, 75, "container", "widget" );
    } )
    .catch( err => {
        console.log("Error:",err);
    } )
;


// drawPredictionWidget( [ exampleResponse, exampleResponse2, exampleResponse3 ], 300, 75, "container", "widget" );



// var gradient = svg.append("svg:defs")
//     .append("svg:linearGradient")
//     .attr("id", "gradient")
//     .attr("x1", "0%")
//     .attr("y1", "0%")
//     .attr("x2", "100%")
//     .attr("y2", "100%")
//     .attr("spreadMethod", "pad");

// // Define the gradient colors
// gradient.append("svg:stop")
//     .attr("offset", "0%")
//     .attr("stop-color", "#a00000")
//     .attr("stop-opacity", 1);

// gradient.append("svg:stop")
//     .attr("offset", "100%")
//     .attr("stop-color", "#aaaa00")
//     .attr("stop-opacity", 1);

