const express = require('express');
const { exec } = require('child_process');
const bodyParser = require('body-parser');
const asyncHandler = require('express-async-handler');

const PORT = process.env.PORT || 5000;

const app = express();
app.use(bodyParser.json());

//node_modules shared
app.use('/static',express.static('node_modules'));

app.use((req, res, next) => {
  console.log("middleware");
  if(!req.url.startsWith("/static/")){
    req.url = `/static${req.url}`;
  }   
  next();
});

const execRscript = async (script, args, output) => {
  return new Promise((resolve, reject) => {
      const command = `${__dirname}/rscript/${script} ${args.map(arg => `"${arg}"`).join(' ')}`;
      const outputPath = `${__dirname}/output/${output}`;
    exec(command, {env: { OUTPUT_FILE: outputPath} } ,(err, stdout, stderr) => {
      if(err) {
          reject(err);
      }
        resolve({ stdout, stderr, outputPath });
    });
  });
};

app.get('/static/pages/:page', (req, res) => {
    const pageName = req.params.page;
    console.log( `asking for${__dirname}` );
    res.sendFile(`${__dirname}/pages/${pageName}.html`);
});

app.post('/rexec', asyncHandler(async (req, res) => {
    const { script, arguments: args, output } = req.body;
  const {
    stdout,
    stderr
  } = await execRscript(script, args, output);
  res.send({
    stdout,
    stderr
  });
}));

app.post('/rexec/csv', asyncHandler(async (req, res) => {
    const { script, arguments: args, output } = req.body;
  const {
    outputPath
  } = await execRscript(script, args, output);
    res.set('Content-Type', 'text/csv');
    res.sendFile(outputPath);
}));

app.get('/static/csv/:name', (req, res) => {
    const csvName = req.params.name;
    res.set('Content-Type', 'text/csv');
    res.sendFile(`${__dirname}/output/${csvName}`);
});

app.get('/static/src/:name', (req, res) => {
    const modulename = req.params.name;
    res.set('content-type', 'text/javascript');
    res.sendFile(`${__dirname}/src/${modulename}`);
});

app.get('/static/module/:srcdir', (req, res) => {
    const modulename = req.params.srcdir;
    res.set('content-type', 'text/javascript');
    res.sendFile(`${__dirname}/node_modules/${modulename}/dist/${modulename}.js`);
});

app.get('/static/css/:name', (req, res) => {
    const fileName = req.params.name;
    res.set('content-type', 'text/css');
    res.sendFile(`${__dirname}/src/${fileName}`);
});

app.get('/static/icon/:name', (req, res) => {
    const iconName = req.params.name;
    res.set('Content-Type', 'image/png');
    res.sendFile(`${__dirname}/icons/${iconName}`);
});

app.get('/static/json/:name', (req, res) => {
    const jsonName = req.params.name;
    res.set('Content-Type', 'text/json');
    res.sendFile(`${__dirname}/output/${jsonName}`);
});


app.use(function (req, res, next) {
  console.error("no matching route")
  res.status(404).send("Sorry can't find that!")
})

app.use(function (err, req, res, next) {
  console.error(err.stack)
  res.status(500).send('Something broke!')
})



app.listen(PORT, () => console.log(`Server ready on port ${PORT}`));
